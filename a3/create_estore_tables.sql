SET DEFINE OFF 

--2 CREATE ESTORE TABLES 
DROP SEQUENCE seq_cus_id; 
Create sequence seq_cus_id
start with 1 
increment by 1 
minvalue 1
maxvalue 10000; 

drop table customer CASCADE CONSTRAINTS PURGE; 
CREATE TABLE customer
(
    cus_id number(3,0) not null, 
    cus_fname varchar2(15) not null, 
    cus_lname varchar2(30) not null,
    cus_street varchar2(30) not null,
    cus_city varchar2(30) not null,
    cus_state char(2) not null, 
    cus_zip number(9) not null, 
    cus_phone number(10) not null, 
    cus_email varchar2(100),
    cus_balance number(7,2), 
    cus_notes varchar2(255), 
    CONSTRAINT pk_customer PRIMARY KEY(cus_id)
); 

DROP SEQUENCE seq_com_id; 
Create sequence seq_com_id
start with 1 
increment by 1 
minvalue 1
maxvalue 10000; 

drop table commodity CASCADE CONSTRAINTS PURGE; 
CREATE TABLE commodity 
(
com_id number not null, 
com_name varchar2(20), 
com_price NUMBER(8,2) NOT NULL, 
cus_notes varchar2(255), 
CONSTRAINT pk_commodity PRIMARY KEY(com_id), 
CONSTRAINT uq_com_name UNIQUE(com_name)
); 

DROP SEQUENCE seq_ord_id; 
Create sequence seq_ord_id 
start with 1 
increment by 1 
minvalue 1
maxvalue 10000; 

drop table "order" CASCADE CONSTRAINTS PURGE; 
CREATE TABLE "order"
(
    ord_id number(4,0) not null, 
    cus_id number, 
    com_id number, 
    ord_num_units number(5,0) NOT NULL, 
    ord_total_cost number(8,2) NOT NULL, 
    ord_notes varchar2(255), 
    CONSTRAINT pk_order PRIMARY KEY (ord_id), 
    CONSTRAINT fk_order_customer 
    FOREIGN KEY (cus_id) 
    REFERENCES customer(cus_id), 
    CONSTRAINT fk_order_customer
    FOREIGN KEY (com_id)
    REFERENCES commodity(com_id), 
    CONSTRAINT check_unit CHECK(ord_num_units > 0 ), 
    CONSTRAINT check_total CHECK(ord_total_cost > 0)
); 

INSERT INTO customer VALUES (seq_cus_id.nextval, 'Beverly', 'Davis', '123 Main St', 'Detroit', 'MI', 48252, 3135551212, 'bdavis@aol.com', 11500.99, 'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Stephen', 'Taylor', '500 Main St', 'Miami', 'FL', 33023, 3056765848, 'st@gmail.com', 25.01, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Mike', 'Will', '300 Main Ave', 'Miami', 'FL', 33045, 3056859485, 'mw@gmail.com', 300.01, 'returning customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Leo', 'Messi', '100 West Ave', 'Miami', 'FL', 33345, 3054758945, 'lm@gmail.com', 40.87, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Lebron', 'James', '200 North St', 'Miami', 'FL', 33234, 4549434949, 'lj@gmail.com', 200.01, 'new customer');
commit; 

INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD', 100.00, NULL); 
INSERT INTO commodity VALUES (seq_com_id.nextval, 'shoes', 200.00, 'Nike'); 
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Chocolate', 1.00, 'Sugar free'); 
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Scrabble', 29.00, NULL); 
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Licorice', 2.00, NULL); 
commit; 

INSERT INTO "order" VALUES (seq_ord_id.nextval, 1,2,50,200,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2,3,30,100,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 4,4,6,300,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5,5,10,400,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3,1,5,500,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1,2,20,600,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2,3,12,700,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3,4,22,800,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 4,5,40,900,NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5,1,45,250,NULL); 
commit; 

select * from customer; 
select * from commodity;
select * from "order"; 





