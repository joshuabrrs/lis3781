
# Adv Database managment

## Joshua Barrios

### Assignment #3 Requirements:

*Sub-Heading:*

1. Tables and data using only sql 
2. sql solutions

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables 


#### Assignment Screenshots:

*Screenshot of SQL code*:

![SQL code Screenshot](sqlcode.png)


*Screenshot of populated tables*:   
![populated tables](poptables.png)   

*Screenshot of populated tables*: 
![populated tables](poptables2.png)    

 *Screenshot of populated tables*:  

![populated tables](poptables3.png)   