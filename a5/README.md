# Adv Database managment

## Joshua Barrios

### Assignment #5 Requirements:


 1. Builds on top of assignment 4
 2. Tables and data created using MS SQL 
 3. Includes 25 unique records for sales table

#### README.md file should include the following items:

* Screenshot of ERD (large diagram so it was hard to capture the full diagram without minimizing fonts)



#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](erd.png)