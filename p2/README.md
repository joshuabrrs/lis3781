
# Adv. Database management

## Joshua Barrios

### Project #2 Requirements:


1. Succesfully configure MongoDB environment
2. Import json file for data
3. Utilize MongoDB commands




#### README.md file should include the following items:


* Screenshot of collections commmand
* Screenshot of restaurant grade command 
* Screenshot of 5 restaurant limit





#### Assignment Screenshots:
|*Screenshot of collections*:        |
|------------------------------------|
|![collection Screenshot](col.png)   |
|                                    | 
|                                    |   

|*Screenshot of restaurant grade*:   |
|------------------------------------|             
|![grade Screenshot](grades.png)     |

|*Screenshot of restaurant limit*:           | 
|--------------------------------------------|   
|![limit Screenshot](limit.png)              |             
