> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Adv. Database managment

## Joshua Barrios

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AAMPS
    - Provide screenshots of installations
    - Create BitBucket repo 
    - Create BitBucket tutorial 
    - Provide git command descriptions 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Learning to create MySQL databases with SHA2 hashing
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Creating and Populating MySQL databases through code (without MySQL Workbench)
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Tables and data populated using MS SQL 
    - ERD created on MS SQL
    - Using MS SQL to Hash values 
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Builds on top of assignment 4
    - Tables and data created using MS SQL 
    - Includes 25 unique records for sales table
6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Succesfully configure MongoDB environment
    - Import json file for data
    - Utilize MongoDB commands
