> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Adv. Database Management

## Joshua Barrios

### Assignment #2 Requirements:

*Sub-Heading:*

1. Tables and insert statements.
2. Include indexes and foreign key SQL statements.
3. Include query resultsets, including grant statements.
4. The following tables should be created and populated with at least 5 records bothlocally and to the CCI server. 

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables



#### Assignment Screenshots:

*Screenshot of SQL code:

![SQL code Screenshot](pt1.png)

*Screenshot of populated tables*:

![Populated Screenshot](pt2.png)
![Populated Screenshot](sha2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
