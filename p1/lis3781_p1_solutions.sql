select 'drop, create, use database, create tables, display data:' as ' '; 
do sleep(5); 

DROP SCHEMA IF EXISTS jgb16; 
CREATE SCHEMA IF NOT EXISTS jgb16; 
SHOW WARNINGS; 
USE jgb16; 



DROP TABLE IF EXISTS person; 
CREATE TABLE IF NOT EXISTS person 
(
    per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, 
    per_ssn BINARY(64) NULL,
    per_salt BINARY(64) NULL,  
    per_fname VARCHAR(15) NOT NULL, 
    per_lname VARCHAR(30) NOT NULL, 
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,  
    per_state CHAR(2) NOT NULL, 
    per_zip INT(9) UNSIGNED ZEROFILL NOT NULL, 
    per_email VARCHAR(100) NOT NULL, 
    per_dob DATE NOT NULL, 
    per_type ENUM('a','c','j') NOT NULL, 
    per_notes VARCHAR(255) NULL, 
    PRIMARY KEY (per_id), 
    UNIQUE INDEX ux_per_ssn (per_ssn ASC)
        
)
ENGINE InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS; 



DROP TABLE IF EXISTS attorney; 
CREATE TABLE IF NOT EXISTS attorney
(
    per_id SMALLINT UNSIGNED NOT NULL, 
    aty_start_date DATE NOT NULL, 
    aty_end_date DATE NULL DEFAULT NULL, 
    aty_hourly_rate DECIMAL(5,2) UNSIGNED NOT NULL, 
    aty_years_in_practice TINYINT NOT NULL, 
    aty_notes VARCHAR(255) NULL DEFAULT NULL, 
    PRIMARY KEY (per_id), 

    INDEX idx_per_id (per_id ASC), 

    CONSTRAINT fk_attorney_person
        FOREIGN KEY (per_id)
        REFERENCES person(per_id)
        ON DELETE NO ACTION 
        ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS; 


DROP TABLE IF EXISTS client; 
CREATE TABLE IF NOT EXISTS client
(
    per_id SMALLINT UNSIGNED NOT NULL, 
    cli_notes VARCHAR(255) NULL DEFAULT NULL, 
    PRIMARY KEY (per_id), 

    INDEX idx_per_id(per_id ASC), 

    CONSTRAINT fk_client_person 
        FOREIGN KEY (per_id)
        REFERENCES person(per_id)
        ON DELETE NO ACTION 
        ON UPDATE CASCADE
)

ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS; 



DROP TABLE IF EXISTS court; 
CREATE TABLE IF NOT EXISTS court
(
crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT, 
crt_name VARCHAR(45) NOT NULL, 
crt_street VARCHAR(30) NOT NULL, 
crt_city VARCHAR(30) NOT NULL, 
crt_state CHAR(2) NOT NULL, 
crt_zip INT(9) UNSIGNED ZEROFILL NOT NULL, 
crt_phone BIGINT NOT NULL, 
crt_email VARCHAR(100) NOT NULL, 
crt_url VARCHAR(100) NOT NULL, 
crt_notes VARCHAR(255) NULL, 
PRIMARY KEY (crt_id)
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS; 


DROP TABLE IF EXISTS judge; 
CREATE TABLE IF NOT EXISTS judge 
(
    per_id SMALLINT UNSIGNED NOT NULL, 
    crt_id TINYINT UNSIGNED NULL DEFAULT NULL, 
    jud_salary DECIMAL(8,2) NOT NULL, 
    jud_years_in_practice TINYINT UNSIGNED NOT NULL, 
    jud_notes VARCHAR(255) NULL DEFAULT NULL, 
    PRIMARY KEY (per_id), 

    INDEX idx_per_id(per_id ASC), 
    INDEX idx_crt_id(crt_id ASC), 

    CONSTRAINT fk_judge_person 
        FOREIGN KEY (per_id)
        REFERENCES person(per_id)
        ON DELETE NO ACTION 
        ON UPDATE CASCADE,
    CONSTRAINT fk_judge_court 
        FOREIGN KEY (crt_id)
        REFERENCES court(crt_id)
        ON DELETE NO ACTION 
        ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS; 


DROP TABLE IF EXISTS judge_hist; 
CREATE TABLE IF NOT EXISTS judge_hist
(
jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, 
per_id SMALLINT UNSIGNED NOT NULL, 
jhs_crt_id TINYINT NULL,
jhs_date timestamp NOT NULL default current_timestamp(), 
jhs_type enum('i', 'u', 'd') NOT NULL default 'i', 
jhs_salary DECIMAL(8,2) NOT NULL, 
jhs_notes VARCHAR(255) NULL, 
PRIMARY KEY (jhs_id), 

INDEX idx_per_id(per_id ASC), 

    CONSTRAINT fk_judge_hist_judge
        FOREIGN KEY (per_id)
        REFERENCES judge(per_id)
        ON DELETE NO ACTION 
        ON UPDATE CASCADE
) 
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS; 

DROP TABLE IF EXISTS `case`; 
CREATE TABLE IF NOT EXISTS `case`; 
(
    cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, 
    per_id SMALLINT UNSIGNED NOT NULL, 
    cse_type VARCHAR(45) NOT NULL, 
    cse_description TEXT NOT NULL, 
    cse_start_date DATE NOT NULL, 
    cse_end_date  DATE NULL, 
    cse_notes VARCHAR(255) NULL, 
    PRIMARY KEY(cse_id), 
    INDEX idx_per_id(per_id ASC), 

    CONSTRAINT fk_court_case_judge
     FOREIGN KEY (per_id)
     REFERENCES judge(per_id)
     ON DELETE NO ACTION 
     ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS;

DROP TABLE IF EXISTS bar; 
CREATE TABLE IF NOT EXISTS bar 
(
bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT, 
per_id SMALLINT UNSIGNED NOT NULL, 
bar_name VARCHAR(45) NOT NULL, 
bar_notes VARCHAR(255) NULL, 
PRIMARY KEY(bar_id), 

INDEX idx_per_id(per_id ASC), 

CONSTRAINT fk_bar_attorney
     FOREIGN KEY (per_id)
     REFERENCES attorney(per_id)
     ON DELETE NO ACTION 
     ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS;

DROP TABLE IF EXISTS specialty; 
CREATE TABLE IF NOT EXISTS specialty
(
spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT, 
per_id SMALLINT UNSIGNED NOT NULL, 
spc_type VARCHAR(45) NOT NULL, 
spc_notes VARCHAR(255) NULL, 
PRIMARY KEY(spc_id), 

CONSTRAINT fk_bar_attorney
     FOREIGN KEY (per_id)
     REFERENCES attorney(per_id)
     ON DELETE NO ACTION 
     ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS;

DROP TABLE IF EXISTS assignment; 
CREATE TABLE IF NOT EXISTS assignment 
(
asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, 
per_cid SMALLINT UNSIGNED NOT NULL, 
per_aid SMALLINT UNSIGNED NOT NULL, 
cse_id SMALLINT UNSIGNED NOT NULL, 
asn_notes VARCHAR(255) NULL, 
PRIMARY KEY(asn_id), 

INDEX idx_per_cid(per_cid ASC), 
INDEX idx_per_aid (per_aid ASC), 
INDEX idx_cse_id (cse_id ASC), 

UNIQUE INDEX ux_per_cid_per_aid_cse_id(per_cid ASC, per_aid ASC, cse_id ASC),

CONSTRAINT fk_assign_case
FOREIGN KEY (cse_id)
     REFERENCES `case`(cse_id)
     ON DELETE NO ACTION 
     ON UPDATE CASCADE, 
CONSTRAINT fk_assignment_client 
FOREIGN KEY (per_cid)
     REFERENCES client(per_id)
     ON DELETE NO ACTION 
     ON UPDATE CASCADE, 

CONSTRAINT fk_assignment_attorney
FOREIGN KEY (per_aid)
     REFERENCES attorney(per_id)
     ON DELETE NO ACTION 
     ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS;

DROP TABLE IF EXISTS phone; 
CREATE TABLE IF NOT EXISTS phone 
(
phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, 
per_id SMALLINT UNSIGNED NOT NULL, 
phn_num BIGINT UNSIGNED NOT NULL, 
phn_type ENUM('h', 'c', 'w', 'f') NOT NULL COMMENT 'home, cell, work,fax', 
phn_notes VARCHAR(255) NULL, 
PRIMARY KEY(phn_id), 

INDEX idx_per_id (per_id ASC), 
CONSTRAINT fk_phone_person
FOREIGN KEY (per_id)
REFERENCES person(per_id)
ON DELETE NO ACTION 
ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8 
COLLATE = utf8_unicode_ci; 

SHOW WARNINGS;

START TRANSACTION; 

INSERT INTO person 
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES 
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', 'c', NULL), 
(NULL, NULL, NULL, 'Bruce', 'Wayne', '400 Eastern Drive', 'Gotham', 'NY', 324402456, 'bman@gmail.com', '1968-10-03', 'c', NULL),
(NULL, NULL, NULL, 'Mike', 'West', '300 Nothern Drive', 'Miami', 'FL', 786402222, 'mw@gmail.com', '1973-10-04', 'c', NULL),
(NULL, NULL, NULL, 'John', 'Frank', '200 Westerm Drive', 'Tampa', 'FL', 786305301, 'jf@gmail.com', '1973-10-05', 'c', NULL),
(NULL, NULL, NULL, 'Hank', 'Smith', '100 East Ave', 'Orlando', 'FL', 786305302, 'hs@gmail.com', '1923-10-06', 'c', NULL),
(NULL, NULL, NULL, 'John', 'Mays', '200 South Ave', 'Tallahassee', 'FL', 786305303, 'jm@gmail.com', '1923-10-07', 'c', NULL),
(NULL, NULL, NULL, 'Homer', 'Simpson', '300 North Ave', 'Springfield', 'IL', 786305304, 'hsimpson@gmail.com', '1923-10-08', 'a', NULL),
(NULL, NULL, NULL, 'Ken', 'Ryu', '400 West Ave', 'New York', 'NY', 786305305, 'kr@gmail.com', '1923-10-09', 'a', NULL),
(NULL, NULL, NULL, 'Denzel', 'Curry', '100 West Lane', 'Los Angeles', 'CA', 786305306, 'dz@gmail.com', '1923-10-10', 'a', NULL),
(NULL, NULL, NULL, 'Jay', 'Z', '200 North Lane', 'Brooklyn', 'NY', 786305307, 'jayz@gmail.com', '1923-10-11', 'a', NULL),
(NULL, NULL, NULL, 'Tame', 'Impala', '300 East Lane', 'Atlanta', 'GA', 786305308, 'ti@gmail.com', '1923-10-12', 'a', NULL),
(NULL, NULL, NULL, 'Luke', 'Cage', '400 South Lane', 'Key West', 'FL', 786305309, 'lcage@gmail.com', '1923-10-13', 'j', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker', '100 Main St', 'Manhattan', 'NY', 786305310, 'pp@gmail.com', '1923-10-14', 'j', NULL),
(NULL, NULL, NULL, 'Morgan', 'Freeman', '200 Main St', 'Syracuse', 'NY', 786305311, 'mf@gmail.com', '1923-10-15', 'j', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark', '300 Main St', 'NYC', 'NY', 786305312, 'ts@gmail.com', '1923-10-16', 'j', NULL); 

COMMIT; 

START TRANSACTION; 

INSERT INTO phone 
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES 
(NULL, 1, 8032288820, 'c', NULL), 
(NULL, 2, 8032288821, 'h', NULL), 
(NULL, 3, 8032288822, 'w', 'has two office numbers'), 
(NULL, 4, 8032288823, 'w', NULL), 
(NULL, 5, 8032288824, 'f', 'fax not working'), 
(NULL, 6, 8032288825, 'c', 'prefers home'), 
(NULL, 7, 8032288826, 'h', NULL), 
(NULL, 8, 8032288827, 'w', NULL), 
(NULL, 9, 8032288828, 'w', 'work fax number'), 
(NULL, 10, 8032288829, 'h', 'prefers cell phone calls'), 
(NULL, 11, 8032288830, 'c', 'best number to reach'), 
(NULL, 12, 8032288831, 'f', 'call during lunch'), 
(NULL, 13, 8032288832, 'w', 'prefers cell phone'), 
(NULL, 14, 8032288833, 'w', NULL), 
(NULL, 15, 8032288834, 'f', 'use for faxing legal calls'); 

COMMIT; 

START TRANSACTION; 

INSERT INTO client 
(per_id, cli_notes)
VALUES
(1, NULL), 
(2, NULL), 
(3, NULL), 
(4, NULL), 
(5, NULL);

COMMIT; 

START TRANSACTION; 

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES 
(6, '2006-06-12', NULL, 85, 5, NULL), 
(7, '2003-08-20', NULL, 130, 28, NULL), 
(8, '2009-12-12', NULL, 70, 17, NULL), 
(9, '2008-06-08', NULL, 78, 13, NULL), 
(10, '2011-09-12', NULL, 60, 24, NULL); 

COMMIT; 

START TRANSACTION; 

INSERT INTO bar 
(bar_id, per_id, bar_name, bar_notes)
VALUES 
(NULL, 6,'Florida bar', NULL), 
(NULL, 7,'Alabama bar', NULL), 
(NULL, 8,'Georgia bar', NULL), 
(NULL, 9,'Michigan bar', NULL), 
(NULL, 10,'South Carolina bar', NULL), 
(NULL, 6,'Montana bar', NULL), 
(NULL, 7,'Arizona bar', NULL), 
(NULL, 8,'Nevada bar', NULL), 
(NULL, 9,'New York bar', NULL), 
(NULL, 10,'New York bar', NULL), 
(NULL, 6,'Mississippi bar', NULL), 
(NULL, 7,'California bar', NULL), 
(NULL, 8,'Illinois bar', NULL), 
(NULL, 9,'Tallahassee bar', NULL), 
(NULL, 10,'Oaala bar', NULL);

COMMIT; 

START TRANSACTION; 

INSERT INTO specialty
(spc_id, per_id, spc_type, spc_notes)
VALUES 
(NULL, 6, 'business', NULL), 
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankkruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'judicial', NULL),
(NULL, 6, 'environmental', NULL),
(NULL, 7, 'criminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL); 

COMMIT; 

START TRANSACTION; 

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES 
(NULL, 'leon county circuit court', '301 south monroe street', 'Tallahassee', 'FL', 323035292, 8506065500,'lccc@us.fl.gov', 'http://www.leoncountycircuitcourt.gov/', NULL), 
(NULL, 'leon county traffic court', '1921 thomasville road', 'Tallahassee', 'FL', 323035292, 8506065501,'lctc@us.fl.gov', 'http://www.leoncountytrafficcourt.gov/', NULL),
(NULL, 'florida supreme court', '500 south duval street', 'Tallahassee', 'FL', 323035292, 8506065502,'fcs@us.fl.gov', 'http://www.floridasupremecourt.org/', NULL),
(NULL, 'orange county courthouse', '424 north orange avenue', 'Orlando', 'FL', 328012248, 8506065503,'occ@us.fl.gov', 'http://www.ninthcircuit.org/', NULL),
(NULL, 'fifth district court of appeal', '300 south beach street', 'Daytona beach', 'FL', 321158763, 8506065504,'5dca@us.fl.gov', 'http://www.5dca.org/', NULL);
COMMIT; 
START TRANSACTION; 

INSERT INTO judge 
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES 
(11,5, 150000,10,NULL), 
(12,4, 185000,3,NULL), 
(13,4, 135000,2,NULL), 
(14,3, 170000,6,NULL), 
(15,1, 120000,1,NULL); 

COMMIT; 

START TRANSACTION; 

INSERT INTO judge_hist 
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES 
(NULL, 11, 3, '2009-01-16', 'i', 130000, NULL), 
(NULL, 12, 2, '2010-05-27', 'i', 140000, NULL), 
(NULL, 13, 5, '2000-01-02', 'i', 115000, NULL), 
(NULL, 13, 4, '2005-07-05', 'i', 135000, NULL), 
(NULL, 14, 4, '2008-12-09', 'i', 155000, NULL), 
(NULL, 15, 1, '2011-03-17', 'i', 120000, 'freshman justice'),
(NULL, 11, 5, '2010-07-05', 'i', 150000, 'assigned to another court'),
(NULL, 12, 4, '2012-10-08', 'i', 165000, 'became chief justice '),
(NULL, 14, 3, '2009-04-19', 'i', 170000, 'reassigned to court');

COMMIT; 

START TRANSACTION; 

INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil', 'client says logo is used without rights', '2010-09-09', NULL, 'copyright infrigment'), 
(NULL, 12, 'criminal', 'client is charged with assualt', '2009-11-18', '2010-12-23', 'assault'),
(NULL, 14, 'civil', 'client broke ankle in store', '2008-09-09', NULL, 'slip and fall'), 
(NULL, 11, 'criminal', 'client stole TVs from job', '2010-05-02', NULL, 'grand theft'), 
(NULL, 13, 'criminal', 'client found with drugs', '2011-06-05', NULL, 'posession of narcotics'), 
(NULL, 14, 'civil', 'client alleges newspaper lied about them', '2007-01-09', NULL, 'defamation'), 
(NULL, 12, 'criminal', 'client did a bad thing', '2008-06-09', NULL, 'bad thing'), 
(NULL, 15, 'civil', 'client declares bankcruptcy', '2012-03-09', NULL, 'bankruptcy');

COMMIT; 
START TRANSACTION; 

INSERT INTO assignment 
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES 
(NULL, 1, 6,7, NULL), 
(NULL, 2, 6,6, NULL), 
(NULL, 3, 7,2, NULL), 
(NULL, 4, 8,2, NULL), 
(NULL, 5, 9,5, NULL), 
(NULL, 1, 10,1, NULL), 
(NULL, 2, 6,3, NULL), 
(NULL, 3, 7,8, NULL), 
(NULL, 4, 8,8, NULL), 
(NULL, 5, 9,8, NULL), 
(NULL, 4, 10,4, NULL); 

COMMIT; 






