

# Adv. Database Management

## Joshua Barrios

### Project 1 Requirements:

*Sub-Heading:*

1. Include ERD
4. The following tables should be created and populated with at least 15 records

#### README.md file should include the following items:

* Screenshot of ERD



#### Assignment Screenshots:

*Screenshot of SQL code:

![SQL code Screenshot](pt1.png)

*Screenshot of populated tables*:

![Populated Screenshot](pt2.png)




