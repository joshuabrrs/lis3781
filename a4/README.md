
# Adv Database managment

## Joshua Barrios

### Assignment #4 Requirements:


1. Tables and data populated using MS SQL 
2. ERD created on MS SQL
3. Using MS SQL to Hash values 

#### README.md file should include the following items:

* Screenshot of ERD (large diagram so it was hard to capture the full diagram without minimizing fonts)



#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](erd.png)
