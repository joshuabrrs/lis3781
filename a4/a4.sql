SET ANSI_WARNINGS ON; 
GO 

use master; 
GO 

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jgb16')
DROP DATABASE jgb16; 
GO 


IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jgb16')
CREATE DATABASE jgb16; 
GO 

use jgb16; 
GO 

-- Table person 

IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL 
DROP TABLE dbo.person; 
GO 

CREATE TABLE dbo.person 
(
    per_id SMALLINT not null identity(1,1), 
    per_ssn binary(64) NULL, 
    per_salt binary(64) NULL, 
    per_fname VARCHAR(15) NOT NULL, 
    per_lname VARCHAR(30) NOT NULL, 
    per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m', 'f')), 
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL, 
    per_city VARCHAR(30) NOT NULL, 
    per_state CHAR(2) NOT NULL DEFAULT 'FL', 
    per_zip int NOT NULL check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'), 
    per_email VARCHAR(100) NULL, 
    per_type CHAR(1) NOT NULL CHECK (per_type IN('c', 's')), 
    per_notes VARCHAR(45) NULL, 
    PRIMARY KEY (per_id), 

    CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)
); 

-- table phone 

IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone; 
GO 

CREATE TABLE dbo.phone 
(
    phn_id SMALLINT NOT NULL identity(1,1), 
    per_id SMALLINT NOT NULL, 
    phn_num bigint NOT NULL check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'), 
    phn_type char(1) NOT NULL CHECK (phn_type IN('h', 'c', 'w', 'f')), 
    phn_notes VARCHAR(255) NULL, 
    PRIMARY KEY (phn_id), 

    CONSTRAINT fk_phone_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
); 

-- Table customer 

IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL 
DROP TABLE dbo.customer; 
GO 

CREATE TABLE dbo.customer
(
    per_id SMALLINT not null, 
    cus_balance decimal(7,2) NOT NULL check (cus_balance >= 0), 
    cus_total_sales decimal(7,2) NOT NULL check (cus_total_sales >= 0), 
    cus_notes VARCHAR(45) NULL, 
    PRIMARY KEY (per_id), 

    CONSTRAINT fk_customer_person 
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
); 

-- Table slsrep 

IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep; 
GO 

CREATE TABLE dbo.slsrep
(
    per_id SMALLINT not null, 
    srp_yr_sales_goal decimal(8,2) NOT NULL check (srp_yr_sales_goal >= 0), 
    srp_ytd_sales decimal(8,2) NOT NULL check (srp_ytd_sales >= 0), 
    srp_ytd_comm decimal(8,2) NOT NULL check (srp_ytd_comm >= 0), 
    srp_notes VARCHAR(45) NULL, 
    PRIMARY KEY (per_id), 

    CONSTRAINT fk_slsrep_person 
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
); 

-- TABLE SRP_HIST 

IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL 
DROP TABLE dbo.srp_hist; 
GO 

CREATE TABLE dbo.srp_hist
(
    sht_id SMALLINT not null identity(1,1), 
    per_id SMALLINT not null, 
    sht_type char(1) not null CHECK (sht_type IN('i', 'u', 'd')), 
    sht_modified datetime not null, 
    sht_modifier varchar(45) not null default system_user, 
    sht_date date not null default getDate(), 
    sht_yr_sales_goal decimal(8,2) NOT NULL check (sht_yr_sales_goal >= 0), 
    sht_yr_total_sales decimal(8,2) NOT NULL check (sht_yr_total_sales >= 0), 
    sht_yr_total_comm decimal(7,2) NOT NULL check (sht_yr_total_comm >= 0), 
    sht_notes VARCHAR(45) NULL, 
    PRIMARY KEY (sht_id), 

    CONSTRAINT fk_srp_hist_slsrep
    FOREIGN KEY (per_id)
    REFERENCES dbo.slsrep (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE

); 
-- table contact 
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL 
DROP TABLE dbo.contact; 
GO

CREATE TABLE dbo.contact 
(
    cnt_id int NOT NULL identity(1,1), 
    per_cid smallint NOT NULL, 
    per_sid smallint NOT NULL, 
    cnt_date datetime NOT NULL, 
    cnt_notes varchar(255) NULL, 
    PRIMARY KEY (cnt_id), 

    CONSTRAINT fk_contact_customer
        FOREIGN KEY (per_cid)
        REFERENCES dbo.customer (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE, 

    CONSTRAINT fk_contact_slsrep 
    FOREIGN KEY (per_sid)
    REFERENCES dbo.slsrep (per_id)
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION
); 

IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order]; 
GO 

CREATE TABLE dbo.[order]
(
   ord_id int NOT NULL identity(1,1), 
   cnt_id int NOT NULL, 
   ord_placed_date DATETIME NOT NULL, 
   ord_filled_date DATETIME NULL, 
   ord_notes VARCHAR(255) NULL, 
   PRIMARY KEY (ord_id), 

   CONSTRAINT fk_order_contact 
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact(cnt_id) 
    ON DELETE CASCADE
    ON UPDATE CASCADE
); 
-- table store

IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL 
DROP TABLE dbo.store; 
GO 

CREATE TABLE dbo.store
(
    str_id SMALLINT NOT NULL identity(1,1), 
    str_name VARCHAR(45) NOT NULL, 
    str_street VARCHAR(30) NOT NULL, 
    str_city VARCHAR(30) NOT NULL, 
    str_state CHAR(2) NOT NULL DEFAULT 'FL', 
    str_zip int NOT NULL check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'), 
    str_phone bigint NOT NULL check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'), 
    str_email VARCHAR(100) NOT NULL, 
    str_url VARCHAR(100) NOT NULL, 
    str_notes VARCHAR(255) NULL, 
    PRIMARY KEY (str_id)
); 

-- table invoice 
IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL 
DROP TABLE dbo.invoice; 
GO 

CREATE TABLE dbo.invoice(
    inv_id int NOT NULL identity(1,1), 
    ord_id int NOT NULL, 
    str_id SMALLINT NOT NULL, 
    inv_date DATETIME NOT NULL, 
    inv_total DECIMAL(8,2) NOT NULL check (inv_total >= 0), 
    inv_paid bit NOT NULL,
    inv_notes VARCHAR(255) NULL, 
    PRIMARY KEY (inv_id), 

    CONSTRAINT ux_ord_id unique nonclustered (ord_id ASC), 

    CONSTRAINT fk__invoice_order
    FOREIGN KEY (ord_id)
    REFERENCES dbo.[order] (ord_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE,

    CONSTRAINT fk__invoice_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE

);

-- table payment

IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL 
DROP TABLE dbo.payment; 
GO 
CREATE TABLE dbo.payment
(
    pay_id int NOT NULL identity(1,1), 
    inv_id int NOT NULL, 
    pay_date DATETIME NOT NULL, 
    pay_amt DECIMAL(7,2) NOT NULL check (pay_amt >= 0 ), 
    pay_notes VARCHAR(255) NULL, 
    PRIMARY KEY (pay_id), 

    CONSTRAINT fk_payment_invoice 
    FOREIGN KEY (inv_id)
    REFERENCES dbo.invoice (inv_id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
); 
-- Table vendor 
IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL 
DROP TABLE dbo.vendor; 
GO 

CREATE TABLE dbo.vendor(
    ven_id SMALLINT NOT NULL identity(1,1), 
    ven_name VARCHAR(45) NOT NULL, 
    ven_street VARCHAR(30) NOT NULL, 
    ven_city VARCHAR(30) NOT NULL, 
    ven_state CHAR(2) NOT NULL DEFAULT 'FL', 
    ven_zip int NOT NULL check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' ), 
    ven_phone bigint NOT NULL check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NULL, 
    ven_url VARCHAR(100) NULL, 
    ven_notes VARCHAR(255) NULL, 
    PRIMARY KEY (ven_id) 
); 

--table product 
IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL 
DROP TABLE dbo.product; 
GO 

CREATE TABLE dbo.product 
(
    pro_id SMALLINT NOT NULL identity(1,1), 
    ven_id SMALLINT NOT NULL, 
    pro_name VARCHAR(30) NOT NULL, 
    pro_descript VARCHAR(45) NULL, 
    pro_weight FLOAT NOT NULL check (pro_weight >= 0), 
    pro_qoh SMALLINT NOT NULL check (pro_qoh >= 0), 
    pro_cost DECIMAL(7,2) NOT NULL check (pro_cost >= 0), 
    pro_price DECIMAL(7,2) NOT NULL check (pro_price >= 0),
    pro_discount DECIMAL(3,0) NULL, 
    pro_notes VARCHAR(255) NULL, 
    PRIMARY KEY (pro_id), 

    CONSTRAINT fk_product_vendor
    FOREIGN KEY (ven_id)
    REFERENCES dbo.vendor (ven_id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE

); 

-- table product_hist 
IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL 
DROP TABLE dbo.product_hist; 
GO 

CREATE TABLE dbo.product_hist
(
    pht_id int NOT NULL identity(1,1), 
    pro_id SMALLINT NOT NULL, 
    pht_date DATETIME NOT NULL, 
    pht_cost DECIMAL(7,2) NOT NULL check (pht_cost >= 0), 
    pht_price DECIMAL(7,2) NOT NULL check (pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL, 
    pht_notes VARCHAR(255) NULL, 
    PRIMARY KEY (pht_id), 

    CONSTRAINT fk_product_hist_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE

); 

-- table order_line 

IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL 
DROP TABLE dbo.order_line; 
GO 

CREATE TABLE dbo.order_line
(
    oln_id int NOT NULL identity(1,1), 
    ord_id int NOT NULL, 
    pro_id SMALLINT NOT NULL, 
    oln_qty SMALLINT NOT NULL check (oln_qty >= 0), 
    oln_price DECIMAL(7,2) NOT NULL check (oln_price >= 0),
    oln_notes VARCHAR(255) NULL, 
    PRIMARY KEY (oln_id), 

    CONSTRAINT fk_order_line_order
    FOREIGN KEY (ord_id)
    REFERENCES dbo.[order] (ord_id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE,

    CONSTRAINT fk_order_line_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
); 

SELECT * FROM information_schema.tables; 


SELECT HASHBYTES('SHA2_512', 'test'); 

SELECT len(HASHBYTES('SHA2_512', 'test')); 

INSERT INTO dbo.person 
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 southern drive', 'Rochester', 'NY', '324402222', 'sr@gmail.com', 's', NULL), 
(2, NULL, 'Frank', 'Ocean', 'm', '1980-10-01', '5 Main St', 'NYC', 'NY', '572939492', 'fo@gmail.com', 's', NULL),
(3, NULL, 'Mike', 'Turner', 'm', '1990-09-02', '10 Main ave ', 'Miami', 'FL', '305678942', 'mt@gmail.com', 's', NULL),
(4, NULL, 'Jonah', 'Smith', 'm', '1988-10-07', '1 West Ave', 'Miami', 'FL', '579087123', 'js@gmail.com', 'c', NULL),
(5, NULL, 'Loki', 'Thor', 'm', '1980-03-11', '2 East Ave', 'Tampa', 'FL', '305123456', 'lt@gmail.com', 'c', NULL),
(6, NULL, 'Hana', 'Reyes', 'f', '1990-05-12', '3 South Ave', 'Miami', 'FL', '305678493', 'hr@gmail.com', 'c', NULL),
(7, NULL, 'John', 'Luke', 'm', '1983-08-07', '4 North Ave', 'Seattle', 'WA', '572939495', 'jl@gmail.com', 's', NULL),
(8, NULL, 'Ryan', 'Sea', 'm', '1988-03-09', '6 Main Ave', 'Tampa', 'FL', '305908765', 'rs@gmail.com', 's', NULL),
(9, NULL, 'Paul', 'Leo', 'm', '1985-08-10', '7 Main Ave', 'Seattle', 'WA', '572939496', 'pl@gmail.com', 's', NULL),
(10, NULL, 'Lora', 'Croft', 'f', '1940-02-08', '8 Main Ave', 'Seattle', 'WA', '572939497', 'lc@gmail.com', 's', NULL);
GO
-- ssn
CREATE PROC dbo.CreatePersonSSN 
AS
BEGIN

    DECLARE @salt binary(64); 
    DECLARE @ran_num int; 
    DECLARE @ssn binary(64); 
    DECLARE @x INT, @y INT; 
    SET @x = 1; 

    SET @y=(select count(*) from dbo.person); 

        WHILE(@x <= @y)
        BEGIN 

        SET @salt=CRYPT_GEN_RANDOM(64); 
        SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; 
        SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));

        update dbo.person
        set per_ssn=@ssn, per_salt=@salt
        where per_id=@x; 

        SET @x = @x + 1; 

        END; 

END; 
GO 

exec dbo.CreatePersonSSN 

-- table slsrep 

INSERT INTO dbo.slsrep 
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES 
(1, 100000, 60000, 1800, NULL), 
(2, 80000, 35000, 3500, NULL), 
(3, 150000, 84000, 9650, 'Great salesperson'), 
(4, 125000, 87000, 15200, NULL), 
(5, 98000, 43000, 8750, NULL); 

select * from dbo.slsrep; 

-- table customer 

INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES 
(6, 120, 14789, NULL), 
(7, 98, 250, NULL), 
(8, 0, 4789, 'always pays'), 
(9, 500, 782, NULL), 
(10, 580, 13789, NULL); 
select * from dbo.customer;

-- table customer 
INSERT INTO dbo.contact 
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1,6,'1999-01-01', NULL), 
(2,6,'2001-01-01', NULL), 
(3,7,'2002-08-09', NULL), 
(4,7,'2002-09-10', NULL), 
(5,7,'2003-07-09', NULL);
select * from dbo.contact;

-- table order
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES 
(1, '2010-11-23', '2010-12-24', NULL), 
(2, '2010-11-24', '2010-12-25', NULL),
(3, '2010-11-25', '2010-12-26', NULL),
(4, '2010-11-26', '2010-12-27', NULL),
(5, '2010-11-27', '2010-12-28', NULL);
select * from dbo.[order];

-- Table store 
INSERT INTO dbo.store 
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES 
('Walgreens', '1 Main St', 'Miami', 'FL', '123456785', '3050981235', 'info@walgreens.com', 'walgreens.com', NULL), 
('CVS', '2 Main St', 'Tampai', 'FL', '123456786', '3050981236', 'info@cvs.com', 'cvs.com', NULL), 
('Lowes', '3 Main St', 'Orlando', 'FL', '123456787', '3050981237', 'info@lowes.com', 'lowes.com', NULL), 
('walmart', '4 Main St', 'Miami', 'FL', '123456788', '3050981238', 'info@walmart.com', 'walmart.com', NULL), 
('Dollar Tree', '5 Main St', 'Miami', 'FL', '123456789', '3050981239', 'info@dollartree.com', 'dollartree.com', NULL); 
select * from dbo.store;
-- table invoice 
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES 
(1, 1, '2001-05-03', 50.30, 0, NULL), 
(2, 1, '2001-05-04', 100, 0, NULL), 
(3, 2, '2001-05-05', 60.00, 1, NULL), 
(4, 3, '2001-05-06', 99.37, 1, NULL), 
(5, 4, '2001-05-07', 27.50, 0, NULL); 
select * from dbo.invoice; 

-- table vendor 
INSERT INTO dbo.vendor 
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES 
('Sysco', '1 Main Ave', 'Miami', 'FL', '123456780', '3059871230', 'hello@sysco.com', 'sysco.com', NULL), 
('Sprint', '2 Main Ave', 'Miami', 'FL', '123456781', '3059871231', 'hello@sprint.com', 'sprint.com', NULL), 
('GE', '3 Main Ave', 'Miami', 'FL', '123456782', '3059871232', 'hello@GE.com', 'GE.com', NULL), 
('Cisco', '4 Main Ave', 'Miami', 'FL', '123456783', '3059871233', 'hello@cisco.com', 'cisco.com', NULL), 
('GoodYear', '5 Main Ave', 'Miami', 'FL', '123456784', '3059871234', 'hello@goodyear.com', 'goodyear.com', NULL); 

select * from dbo.vendor;

-- table product 

INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES 
(1, 'hammer','', 2.5, 45, 4.99, 7.99, 30, NUll), 
(2, 'screwdriver','', 1.8, 120, 1.99, 3.49, NULL, NUll), 
(3, 'pail','', 2.8, 48, 3.89, 7.99, 40, NUll), 
(4, 'cooking oil','peanut oil', 15, 19, 19.99, 28.99, NULL, 'gallons'), 
(5, 'frying pan','', 3.5, 178, 8.45, 13.99, 50, 'on sale price');
select * from dbo.product;

-- table order_line 
INSERT INTO dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1,2,10,8.0, NULL), 
(2,3,7,9.88, NULL), 
(3,4,3,8.99, NULL), 
(4,1,2,6.99, NULL), 
(5,5,13,58.99, NULL); 
select * from dbo.order_line;

INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1,'2005-01-02 11:50:01', 4.99, 7.99, 30,'discounted' ), 
(2,'2005-01-03 11:50:02', 1.99, 3.49, NUll,NULL), 
(3,'2005-01-04 11:50:03', 3.89, 7.99, 40,NULL ), 
(4,'2005-01-05 11:50:04', 19.99, 28.99, NUll,NULL ), 
(5,'2005-01-06 11:50:05', 8.45, 17.99, 60,'discounted' );
 select * from dbo.product_hist;

 -- table srp_hist 

 INSERT INTO dbo.srp_hist
 (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
 VALUES 
(1,'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL), 
(2,'i', getDate(), SYSTEM_USER, getDate(), 150000, 1750000, 17500, NULL), 
(3,'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL), 
(4,'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL), 
(5,'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL); 
 select * from dbo.srp_hist;